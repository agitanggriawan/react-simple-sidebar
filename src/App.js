import React from 'react';
// import logo from './logo.svg';
import Admin from './component/Admin';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import './App.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" component={Admin} />
      </Switch>
    </Router>
  );
}

export default App;
